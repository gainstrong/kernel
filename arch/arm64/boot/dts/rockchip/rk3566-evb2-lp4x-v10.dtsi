// SPDX-License-Identifier: (GPL-2.0+ OR MIT)
/*
 * Copyright (c) 2020 Rockchip Electronics Co., Ltd.
 *
 */

/dts-v1/;

#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/display/media-bus-format.h>
#include <dt-bindings/pinctrl/rockchip.h>
#include "rk3566.dtsi"
#include "rk3566-evb.dtsi"

/ {
	model = "Rockchip RK3566 EVB2 LP4X V10 Board";
	compatible = "rockchip,rk3566-evb2-lp4x-v10", "rockchip,rk3566";
};

&combphy1_usq {
	status = "okay";
};

&combphy2_psq {
	status = "okay";
};


/*
 * video_phy1 needs to be enabled
 * when dsi1 is enabled
 */
&dsi1 {
	status = "disabled";
};

&dsi1_in_vp0 {
	status = "disabled";
};

&dsi1_in_vp1 {
	status = "disabled";
};

&gmac1 {
	phy-mode = "rmii";
	clock_in_out = "output";
	assigned-clocks = <&cru SCLK_GMAC1_RX_TX>, <&cru SCLK_GMAC1>;
	assigned-clock-parents = <&cru SCLK_GMAC1_RMII_SPEED>;
	assigned-clock-rates = <0>, <50000000>;
	snps,reset-gpio = <&gpio3 RK_PC2 GPIO_ACTIVE_LOW>;
	snps,reset-active-low;
	snps,reset-delays-us = <0 20000 100000>;
	pinctrl-names = "default";
	pinctrl-0 = <&gmac1m0_miim &gmac1m0_clkinout &gmac1m0_rx_bus2
		&gmac1m0_tx_bus2 &gmac1m0_rx_er>;
	phy-handle = <&rmii_phy1>;
	status = "okay";
};

&mdio1 {
	rmii_phy1: phy@0 {
		compatible = "ethernet-phy-ieee802.3-c22";
		reg = <0x0>;
	};
};

&mdio1 {
	rgmii_phy0: phy@0 {
		compatible = "ethernet-phy-ieee802.3-c22";
		reg = <0x0>;
	};
};


&video_phy0 {
	status = "okay";
};

&video_phy1 {
	status = "disabled";
};

&pinctrl {
	sdio-pwrseq {
		wifi_enable_h: wifi-enable-h {
			rockchip,pins = <2 RK_PB1 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};
};

&rkcif {
	status = "okay";
};


&rkcif_mmu {
	status = "okay";
};

&rkisp {
	status = "okay";
};

&rkisp_mmu {
	status = "okay";
};

&route_dsi0 {
	status = "okay";
	connect = <&vp1_out_dsi0>;
};

&sdmmc2 {
	status = "disabled";
};

&sdmmc1 {
	max-frequency = <150000000>;
	supports-sdio;
	bus-width = <4>;
	disable-wp;
	cap-sd-highspeed;
	cap-sdio-irq;
	keep-power-in-suspend;
	mmc-pwrseq = <&sdio_pwrseq>;
	non-removable;
	pinctrl-names = "default";
	pinctrl-0 = <&sdmmc1_bus4 &sdmmc1_cmd &sdmmc1_clk>;
	sd-uhs-sdr104;
	status = "okay";
};

&sdio_pwrseq {
	reset-gpios = <&gpio2 RK_PB1 GPIO_ACTIVE_LOW>;
};

&spdif_8ch {
	status = "okay";
	pinctrl-names = "default";
	pinctrl-0 = <&spdifm1_tx>;
};

&uart1 {
	status = "okay";
	pinctrl-names = "default";
	pinctrl-0 = <&uart1m0_xfer &uart1m0_ctsn>;
};

